package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
        //Operators
        // Arithmetic: -, +, /, *, %
        // Comparison: <, >, >=, <=, ==, !=
        // Logical: &&, ||, !
        // Re/assignment: =, +=, -+, *=, /=, %=
        // Increment/Decrement: i++, ++i, i--, --i

        Scanner scanner = new Scanner(System.in);
        int num1;

        System.out.print("Provide a number: ");
        num1 = scanner.nextInt();

        if(num1%5==0) System.out.println(num1+" is divisble by 5");
        else System.out.println(num1+" is not divisble by 5");

        // Short Circuiting
        // applicable only to AND & OR operators wherin if statement/s or other control exits early by ensuring safety operation or for efficiency
        // if(condition1 || condition2 || condition3 || condition4)

        // Ternary Operator
        int num2 = 24;
        Boolean result = (num2>0) ? true : false;
        System.out.println(result);

        // Switch Statement
        System.out.println("Enter number: ");
        int directionValue = scanner.nextInt();

        switch(directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
        }

    }
}