package com.zuitt.example;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static javax.swing.UIManager.put;

public class Array {
    public static void main(String[] args){

        // Array declaration
        // int array - default value = 0
        // string array - default value = null
        int[] intArray = new int[5];

        // Prints memory address
        System.out.println(intArray);

        intArray[0] = 200;
        intArray[1] = 122;
        intArray[2] = 322;

        // Default does not contain way to print out array
        // Imports Arrays package to enable print method
        System.out.println(Arrays.toString(intArray));

        int[] initArray = {1,2,3,4,5};
        System.out.println(Arrays.toString(initArray));
        System.out.println(initArray.length);

        // Array methods
        // sort
        Arrays.sort(intArray);
        System.out.println(Arrays.toString(intArray));

        // multidimensional array
        int[][] multiInt = new int[5][5];
        int counter = 0;
        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++){
                multiInt[i][j] = counter++;
            }
        }
        System.out.println(Arrays.deepToString(multiInt));

        String[][] classroom = new String[3][3];
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        System.out.println(Arrays.deepToString(classroom));

        //ArrayList
        //resizable arrays but does not allow primitive types
        //requires using wrappers to use primitive types
        //ArrayList<T> identifier = new ArrayList<T>();
        // <T> - specifies that list can only have one type of object
        ArrayList<Integer> newArray = new ArrayList<Integer>();

        newArray.add(15);
        newArray.add(30);
        System.out.println(newArray);
        System.out.println(newArray.get(1));

        ArrayList<String> newString = new ArrayList<String>(Arrays.asList("jave", "mike"));
        System.out.println(newString);

        // set - update
        newString.set(newString.indexOf("jave"), "Juan");
        System.out.println(newString);

        // delete
        newString.remove("Juan");
        System.out.println(newString);
        newString.add("Juan");
        newString.add("Juan");
        System.out.println(newString);
        System.out.println(newString.size());
        newString.clear();
        System.out.println(newString);

        // Hashmap
        HashMap<String, String> jobPosition = new HashMap<String, String>();
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
        jobPosition.put("Student", "John");
        System.out.println(jobPosition);
        System.out.println(jobPosition.get("Student"));

        jobPosition.replace("Student", "Johnson");
        System.out.println(jobPosition);

        jobPosition.remove("Dreamer");

        System.out.println(jobPosition);
        System.out.println(jobPosition.keySet());
        System.out.println(jobPosition.values());
        jobPosition.clear();
        System.out.println(jobPosition);

        HashMap<String, String> jobPosition2 = new HashMap<String, String>(){
            {
                put("Teacher", "Harold");
                put("Artists", "Jane");
            }
        };
        System.out.println(jobPosition2);



    }
}